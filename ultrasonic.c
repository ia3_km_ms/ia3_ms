#include "ev3api.h"
#include "common.h"
#include "drive_control.h"
#include "linetrace.h"

#define OBJECT_DETECT_DISTANCE 20
#define STEER_RIGHT 50
#define STEER_LEFT -50
#define SPEED 30
#define D_STEER_RIGHT_1ST 100
#define D_STEER_LEFT_1ST -100
#define D_SPEED_1ST 50

#define US_COLOR_DETECT 10

static const uint8_t			REFLECT_BLACK_THRESHOLD		= 20; /* カラーセンサーの黒認識閾値 */

/* 内部関数定義 */
static void dodge_object(void);
static void first_turn(void);

void ultrasonic(void)
{
	int16_t get_sonic = 0;
	
	get_sonic = ev3_ultrasonic_sensor_get_distance( D_PORT_TYPE_ULTRASONIC_SENSOR );
	
	/* 超音波検査で所得した値が指定距離より近づいたら回避運動開始 */
	if( get_sonic <= OBJECT_DETECT_DISTANCE )
	{
		printf("[%s:%d]sonic=%d\n",__func__,__LINE__,get_sonic);
		(void)ev3_speaker_play_tone(NOTE_A4, 100);
		dodge_object();
	}
}

/* 初回旋回時処理 */
/* 障害物接近時の処理なので他の迂回処理と分割して作成(閾値もわける予定) */
static void first_turn(void)
{
	int16_t get_sonic = 0;
	
	/* 暫定：停止処理 遅くなるので将来的には消す予定 */
	drive_speed(0);
	
	// 100ms sleep
	tslp_tsk(100);
	
	/* 暫定：その場で右旋回 遅くなるので将来的には消す予定 */
	drive_steer(D_STEER_RIGHT_1ST);
	drive_speed(D_SPEED_1ST);
	
	/* 90度旋回できたらOK。時間で調整するかジャイロセンサを使うかは後で検討)*/
	while(1)
	{
		/* 距離取得 */
		get_sonic = ev3_ultrasonic_sensor_get_distance( D_PORT_TYPE_ULTRASONIC_SENSOR );
		
		/* 障害物を検知しなくなった */
		if( get_sonic > OBJECT_DETECT_DISTANCE )
		{
			/* 右旋回から再開 */
			drive_steer(STEER_RIGHT);
			drive_speed(SPEED);
			
			/* 初回迂回処理終了 */
			break;
		}
		
		// 1ms sleep
		tslp_tsk(1);
	}
}

static void dodge_object(void)
{
	uint8_t color_detect;
	int16_t get_sonic = 0;
	
	/* 初回迂回処理 */
	first_turn();
	
	while(1)
	{
		/* 距離取得 */
		get_sonic = ev3_ultrasonic_sensor_get_distance( D_PORT_TYPE_ULTRASONIC_SENSOR );
		
		/* 障害物に接近している場合 */
		if( get_sonic <= OBJECT_DETECT_DISTANCE )
		{
			/* 障害物を検知した場合に右に回避 */
			drive_steer( STEER_RIGHT );
		}
		else
		{
			/* 障害物を見失った場合に左に回避 */
			drive_steer( STEER_LEFT );
		}
		
		/* カラーセンサー値取得 */
		color_detect = ev3_color_sensor_get_reflect( D_PORT_TYPE_COLOR_SENSOR );
		
		/* 初回以外の旋回中に黒線を検知した場合に障害物検知終了 */
		if( color_detect <= REFLECT_BLACK_THRESHOLD )
		{
			break;
		}
		
		// 1ms sleep
		tslp_tsk(1);
	}
}
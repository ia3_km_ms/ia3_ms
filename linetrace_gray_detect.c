#include "ev3api.h"

#include "linetrace_gray_detect.h"

/* 初期化時パラメータ */
static int G_GLAY_LOWER_LIMIT = 30;	/* 灰色下限値 */
static int G_GLAY_UPPER_LIMIT = 70;	/* 灰色上限値 */

static int G_GLAY_DETECT_FRAME = 30;

/* 変数 */
static int G_gray_count = 0;

bool linetrace_gray_detect( int reflect )
{
	bool ret = false;
	
	/* 灰色判定時にカウントを増加 */
	if( G_GLAY_LOWER_LIMIT <= reflect && reflect <= G_GLAY_UPPER_LIMIT )
	{
		G_gray_count++;
	}
	else
	{
		G_gray_count = 0;
	}
	
	/* 連続検出回数に到達したらtrueを返す */
	if( G_gray_count >= G_GLAY_DETECT_FRAME )
	{
		/* ビープ鳴動 */
		ev3_speaker_play_tone(NOTE_C4, 1000);
		
		ret = true;
	}
	
	return ret;
}

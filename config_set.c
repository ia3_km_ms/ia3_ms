#include <string.h>
#include "ev3api.h"
#include "common.h"
#include "drive_control.h"
#include "linetrace.h"
#include "linetrace_pid.h"
#include "linetrace_curve_detect.h"
#include "config_set.h"


/*内部関数定義*/
static void btnPushEvent(intptr_t button);			/*ボタンが押された場合のイベントハンドラ*/
static void clearDisplay(void);


/*グローバル変数定義*/
int G_up_button = 0;			/*上ボタンの判定フラグ*/
int G_down_button = 0;			/*下ボタンの判定フラグ*/
int G_left_button = 0;			/*左ボタンの判定フラグ*/
int G_right_button = 0;			/*右ボタンの判定フラグ*/
int G_enter_button = 0;			/*中央ボタンの判定フラグ*/
int G_on_push_event = 0;		/*ボタンが押されたフラグ*/



void conf_set_value(void){
	int setValue[EN_FUNC_ID_MAX] = {0};
	funcTable table[EN_FUNC_ID_MAX] = {
		{EN_FUNC_ID_SET_KP,"set_kp",					-65535,65535,200, linetrace_pid_pat1_set_kp},
		{EN_FUNC_ID_SET_KI,"set_ki",					-65535,65535,200, linetrace_pid_pat1_set_ki},
		{EN_FUNC_ID_SET_KD,"set_kd",					-65535,65535,50,  linetrace_pid_pat1_set_kd},
		{EN_FUNC_ID_SET_BLACK,"set_black",				-65535,65535,20,  linetrace_set_black},
		{EN_FUNC_ID_SET_WHITE,"set_white",				-65535,65535,80,  linetrace_set_white},
		{EN_FUNC_ID_SET_SPEED,"set_speed",				-65535,65535,30,  linetrace_set_speed},
		{EN_FUNC_ID_SET_CV_SPEED,"set_curve_speed",		-65535,65535,20,  linetrace_set_curve_speed},
		{EN_FUNC_ID_SET_TR,"set_curve_turn_ratio",		-65535,65535,50,  linetrace_set_curve_turn_ratio},
		{EN_FUNC_ID_SET_DT_FRAME,"set_curve_dt_frame",	-65535,65535,30,  linetrace_curve_detect_set_detect_frame},
		{EN_FUNC_ID_SET_SPEED_STEP,"speed_step",		-65535,65535,2,   drive_control_set_speed_step},
		{EN_FUNC_ID_SET_TURN_RATIO_STEP,"turn_rato_step",	-65535,65535,2,  drive_control_set_turn_ratio_step},
	};
	
	int countId = 0;														/*関数IDを管理する変数*/
	char nameInfomation[INFO_SIZE] = {'\0'};								/*実際に表示するための関数名用のバッファ*/
	char valueInfomation[INFO_SIZE] = {'\0'};								/*実際に表示するための設定値用のバッファ*/
	int i = 0;																	/* ループカウンタ */
	
	FILE *fp;
	int read_funcValue = 0;
	int ret=0;
//	sprintf(nameInfomation,"Function : %s",table[countId].funcName);		/*初回表示用の関数名を設定*/
//	sprintf(valueInfomation,"Value : %d",setValue[countId]);				/*初回表示用の設定値を設定*/
	/**************************************************
	*	本体画面に表示させようとしたところ、		  *
	*	1行にまとめて表示すると画面外まではみ出して	  *
	*	表示されたので2つに分けて表示する。			  *
	***************************************************/
	
	/*各イベントハンドラ登録*/
	ev3_button_set_on_clicked(UP_BUTTON,btnPushEvent,UP_BUTTON);
	ev3_button_set_on_clicked(DOWN_BUTTON,btnPushEvent,DOWN_BUTTON);
	ev3_button_set_on_clicked(LEFT_BUTTON,btnPushEvent,LEFT_BUTTON);
	ev3_button_set_on_clicked(RIGHT_BUTTON,btnPushEvent,RIGHT_BUTTON);
	ev3_button_set_on_clicked(ENTER_BUTTON,btnPushEvent,ENTER_BUTTON);
	
	/* ファイル読み込み(衛藤さん) */
	fp = fopen("/setting.csv","r");
	
	if(fp != NULL)
	{
		/* ファイルから各変数に変換して代入 */
		while(( ret = fscanf( fp, "%d",  &read_funcValue )) != EOF ) {
			/* 変換して代入された変数を表示 */
			setValue[i] = read_funcValue;
			i++;
			
			if( i >= EN_FUNC_ID_MAX )
			{
				break;
			}
		}
		
		fclose(fp);
	}
	else
	{
		/* デフォルト値の設定 */
		for(i = 0;i < EN_FUNC_ID_MAX; i++)
		{
			setValue[i] = table[i].def;
		}
	}
	
	while(1){
		
		/*上ボタン押下時の処理*/
		if(G_up_button == 1){
			G_up_button = 0;
			if(setValue[countId] < table[countId].max){
				/*設定値を上げる*/
				setValue[countId] += 1;
			}else{
				/*設定値が最大値を超えるとき最小値に設定しなおす*/
				setValue[countId] = table[countId].min;
			}
			
		}
		
		/*下ボタン押下時の処理*/
		if(G_down_button == 1){
			G_down_button = 0;
			if(setValue[countId] > table[countId].min){
				/*設定値を下げる*/
				setValue[countId] -= 1;
			}else{
				/*設定値が最小値を超えるとき最大値に設定しなおす*/
				setValue[countId] = table[countId].max;
			}			
		}
		
		/*左ボタン押下時の処理*/
		if(G_left_button == 1){
			G_left_button = 0;
			if(countId > 0){											/*ガード処理  関数テーブルの範囲外へ行かないように*/
				/*対象の関数を変更、ひとつ前の関数にする*/
				countId--;
			}else{
				countId = (EN_FUNC_ID_MAX - 1);							/*登録されている最後の関数にあわせる*/
			}
		}
		
		/*右ボタン押下時の処理*/
		if(G_right_button == 1){
			G_right_button = 0;
			if(countId < (EN_FUNC_ID_MAX-1) ){
				/*対象の関数を変更、次の関数にする*/
				countId++;
			}else{
				countId = 0;
			}
		}
		
		/*中央ボタン押下時の処理*/
		if(G_enter_button == 1){
			/*テーブルに登録されている関数をコール*/
			G_enter_button = 0;
			break;
		}
		
		tslp_tsk(100);	/*100mSecのディレイ*/
		
		if( G_on_push_event == 1 )
		{
			/* いったん出力内容をクリア*/
			clearDisplay();
			G_on_push_event = 0;
		}
		
		memset(nameInfomation,0,sizeof(nameInfomation));
		memset(valueInfomation,0,sizeof(valueInfomation));
		
		sprintf(nameInfomation, "Function :%d %s",countId, table[countId].funcName);/*現在選択中の関数を文字列バッファに代入*/
		sprintf(valueInfomation,"Value    :%d",setValue[countId]);		/*現在の設定値を文字列バッファに代入*/
		ev3_lcd_draw_string(nameInfomation, 0,10);						/*関数名の表示*/	
		ev3_lcd_draw_string(valueInfomation, 0,30);						/*設定値の表示*/
	}
	
	/* 全関数の設定 */
	for(i = 0;i < EN_FUNC_ID_MAX; i++)
	{
		table[i].func(setValue[i]);
		DEBUG_PRINT("[%d]%s=%d",i,table[i].funcName,setValue[i]);
	}
	
	/* ファイル書き込み(衛藤さん) */
	fp = fopen("/setting.csv","w");
	
	if(fp != NULL)
	{
		for(i = 0;i < EN_FUNC_ID_MAX; i++)
		{
			fprintf(fp, "%d\n", setValue[i]);
		}
		
		fclose(fp);
	}
	
	/* 終わったので音鳴らす */
	ev3_speaker_play_tone(NOTE_C4, 100);
}


static void btnPushEvent(intptr_t button){

	switch(button){
	case UP_BUTTON:
		G_up_button = 1;
		break;
	case DOWN_BUTTON:
		G_down_button = 1;
		break;
	case LEFT_BUTTON:
		G_left_button = 1;
		break;
	case RIGHT_BUTTON:
		G_right_button = 1;
		break;
	case ENTER_BUTTON:
		G_enter_button = 1;
		break;
	default:
		ev3_lcd_draw_string("NO Rule Of Button", 0,10);
	}
	
	G_on_push_event = 1;

}

/* LCD画面の初期化 */
static void clearDisplay( void )
{
	int i;
	
	for( i = 0; i < EV3_LCD_HEIGHT; i++ )
	{
		ev3_lcd_draw_string("                                                                                              ", 0,i);
	}
}

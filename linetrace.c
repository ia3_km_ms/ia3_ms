#include "ev3api.h"

#include "common.h"
#include "drive_control.h"
#include "linetrace_pid.h"
#include "linetrace_curve_detect.h"
#include "linetrace.h"

#define COMPILE_ERROR compile_error

/* コンパイルスイッチ */
#define LINETRACE_PID_PAT_N 1

/* 定数 */
static int			REFLECT_BLACK_THRESHOLD		= 20;	/* 黒判定する反射光のしきい値(メモ：RGBで判定する？) */
static int			REFLECT_WHITE_THRESHOLD		= 80;
static int			LINETRACE_SPEED				= 30;
static int			LINETRACE_CURVE_SPEED		= 20;
static int			LINETRACE_CURVE_TURN_RATIO	= 50;

/* セット関数 */
void linetrace_set_black( int black )
{
	REFLECT_BLACK_THRESHOLD = black;
	linetrace_curve_detect_set_black_threshold( REFLECT_BLACK_THRESHOLD + 10 );
	linetrace_pid_pat1_set_black_target( REFLECT_BLACK_THRESHOLD );
}
void linetrace_set_white( int white )
{
	REFLECT_WHITE_THRESHOLD = white;
	linetrace_curve_detect_set_white_threshold( REFLECT_WHITE_THRESHOLD - 10 );
	linetrace_pid_pat1_set_white_target( REFLECT_WHITE_THRESHOLD );
}
void linetrace_set_speed( int speed ) { LINETRACE_SPEED = speed; }
void linetrace_set_curve_speed( int speed ) { LINETRACE_CURVE_SPEED = speed; }
void linetrace_set_curve_turn_ratio( int ratio ) { LINETRACE_CURVE_TURN_RATIO = ratio; }

/* 変数 */
//static int G_white_reflect = 80;	/* 白領域の反射値(大会当日に確認して調整) */
//static int G_black_reflect = 20;	/* 黒領域の反射値(大会当日に確認して調整) */

static int G_prev_curve_detect = CURVE_DETECT_NONE;

/* ライントレース初期化 */
void linetrace_init(void)
{
	/* パラメータセット */
#if LINETRACE_PID_PAT_N == 0
	linetrace_pid_pat0_set_black_threashold( REFLECT_BLACK_THRESHOLD );
	linetrace_pid_pat0_set_speed( 30 );
	linetrace_pid_pat0_set_steer( 50 );
	
#elif LINETRACE_PID_PAT_N == 1
	//linetrace_pid_pat1_set_black_target( G_black_reflect );
	//linetrace_pid_pat1_set_white_target( G_white_reflect );
	//linetrace_pid_pat1_set_speed( 100 );
	
#else
	/* わざとコンパイルエラーにする */
	COMPILE_ERROR;
#endif
	
	/* PID制御初期化 */
	LINETRACE_PID_PAT_INIT( LINETRACE_PID_PAT_N );
	
	/* カーブ検出初期化 */
	linetrace_curve_detect_init( REFLECT_BLACK_THRESHOLD );
}

/* ライントレース制御 */
void linetrace_control(void)
{
	uint8_t color_sensor_val = 0;
	LINETRACE_PID_PARA para;
	int curve_detect = CURVE_DETECT_NONE;
	
	/* 色取得 */
	color_sensor_val = ev3_color_sensor_get_reflect( D_PORT_TYPE_COLOR_SENSOR );
	
	char buf[256];
	sprintf(buf, "reflect = %u", color_sensor_val);
	ev3_lcd_draw_string(buf, 0, 10);
	
	/* カーブ検出 */
	curve_detect = linetrace_curve_detect( color_sensor_val );
	
	/* 速度・回転角計算 */
	para = LINETRACE_PID_PAT_CALC( LINETRACE_PID_PAT_N, color_sensor_val );
	
	if( curve_detect == CURVE_DETECT_BLACK )
	{
		/* 右に急旋回する */
		drive_turn_ratio( LINETRACE_CURVE_TURN_RATIO );
		drive_speed( LINETRACE_CURVE_SPEED );
		drive_steer( para.steer );
	}
	else if( curve_detect == CURVE_DETECT_WHITE )
	{
		/* 左に急旋回する */
		drive_turn_ratio( -1 * LINETRACE_CURVE_TURN_RATIO );
		drive_speed( LINETRACE_CURVE_SPEED);
		drive_steer( para.steer );
	}
	else
	{
		/* カーブから復帰した時、PID制御の過去状態をリセットする */
		//linetrace_pid_pat1_reset();
		
		/* 速度・回転角計算 */
		//para = LINETRACE_PID_PAT_CALC( LINETRACE_PID_PAT_N, color_sensor_val );
		
		/* 速度・回転角更新 */
		//drive_speed( para.speed );
		drive_turn_ratio( 0 );
		drive_speed( LINETRACE_SPEED );
		drive_steer( para.steer );
	}
	
	/* カーブ検出を更新 */
	G_prev_curve_detect = curve_detect;
}

/* リセット */
void linetrace_reset(void)
{
	LINETRACE_PID_PAT_RESET( LINETRACE_PID_PAT_N );
}

#if 0
void linetrace_control_old(void)
{
	uint8_t color_sensor_val = 0;
	
	printf("linetrace_control %u\n", count);
	count++;
	
	/* 速度設定 */
	drive_speed( 30 );  /* 暫定で100固定 */
	
	color_sensor_val = ev3_color_sensor_get_reflect( COLOR_SENSOR_PORT );
	
	printf("color_sensor_val=%u\n", color_sensor_val);
	
	/* 進行方向の判定 */
	if( color_sensor_val <= REFLECT_BLACK_THRESHOLD )
	{
		/* 黒判定のとき右転回 */
		drive_steer( 50 );  /* 暫定で右50固定 */
	}
	else
	{
		/* 白判定のとき左転回 */
		drive_steer( -50 );  /* 暫定で左50固定 */
	}
}
#endif

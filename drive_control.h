#ifndef DRIVE_CONTROL_H
#define DRIVE_CONTROL_H

int drive_init(void);
void drive_speed(int speed);				/* 速度設定(-100～100) 正は前進、負は後退 */
void drive_steer(int steer);				/* 舵角設定(-100～100) 正は右転回、負は左転回 */
void drive_turn_ratio(int ratio);

/* セット関数 */
void drive_control_set_speed_step( int step );
void drive_control_set_turn_ratio_step( int step );

#endif

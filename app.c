#include "ev3api.h"
#include "app.h"

#include "common.h"
#include "drive_control.h"
#include "linetrace.h"
#include "config_set.h"
#include "ultrasonic.h"
#include "shutdown.h"

/* 暫定 */
static inline void CHECK( const char* str, int val )
{
	printf("%s = %d\n", str, val);
}

void main_task(intptr_t unused) {
	
	ER ret = E_OK; /* 戻り値 */
	int l_ret = 0; /*ローカル関数戻り値 */
	
	conf_set_value();
	
	//CHECK( "ev3_button_set_on_clicked(SHUTDOWN)", ev3_button_set_on_clicked( BACK_BUTTON, button_shutdown_event, BACK_BUTTON ) );
	CHECK( "ev3_sensor_config(COLOR)", ev3_sensor_config( D_PORT_TYPE_COLOR_SENSOR, COLOR_SENSOR ) );
	CHECK( "ev3_sensor_config(ULTRASONIC)", ev3_sensor_config( D_PORT_TYPE_ULTRASONIC_SENSOR , ULTRASONIC_SENSOR ) );
	
	/* 初期化処理：ライントレース機能部 */
	linetrace_init();
	
	l_ret = drive_init();
	if(l_ret != D_IA3_MS_OK)
	{
		DEBUG_PRINT("l_ret=%d\n",ret);
		return;
	}
	
	DEBUG_PRINT("run\n");
	
	/* 走行 */
	while(1)
	{
		/* 障害物回避 */
		//ultrasonic();
		
		/* ライントレース処理 */
		linetrace_control();
		
		// 1ms sleep
		tslp_tsk(1);
	}
	
	DEBUG_PRINT("finish\n");
}

#include "ev3api.h"

#include "linetrace_curve_detect.h"

/* 初期化時パラメータ */
static int G_INNER_CURVE_DETECT_FRAME = 30;	/* 内カーブと判定する連続黒検出回数 */
static int G_BLACK_THRESHOLD = 20;
static int G_WHITE_THRESHOLD = 75;

/* 変数 */
static int G_black_count = 0;
static int G_white_count = 0;

void linetrace_curve_detect_init( int black_threshold )
{
	G_BLACK_THRESHOLD = black_threshold;
}

int linetrace_curve_detect( int reflect )
{
	int ret = CURVE_DETECT_NONE;
	
	/* しきい値未満なら黒カウントを増加 */
	if( reflect < G_BLACK_THRESHOLD )
	{
		G_black_count++;
		G_white_count = 0;
	}
	else if( reflect > G_WHITE_THRESHOLD )
	{
		G_black_count = 0;
		G_white_count++;
	}
	else
	{
		G_black_count = 0;
		G_white_count = 0;
	}
	
	/* 黒連続検出 */
	if( G_black_count >= G_INNER_CURVE_DETECT_FRAME )
	{
		//printf("curve\n");
		
		/* ビープ鳴動 */
		//ev3_speaker_play_tone(NOTE_C4, 500);
		
		ret = CURVE_DETECT_BLACK;
	}
	
	/* 白連続検出 */
	if( G_white_count >= G_INNER_CURVE_DETECT_FRAME )
	{
		//printf("curve\n");
		
		/* ビープ鳴動 */
		//ev3_speaker_play_tone(NOTE_C4, 500);
		
		ret = CURVE_DETECT_WHITE;
	}
	
	char buf[256];
	sprintf(buf, "curve_detect=%4d", ret);
	ev3_lcd_draw_string(buf, 0, 70);
	
	return ret;
}

/* パラメータ設定 */
void linetrace_curve_detect_set_detect_frame(int frame)
{
	G_INNER_CURVE_DETECT_FRAME = frame;
}

void linetrace_curve_detect_set_black_threshold( int black_th )
{
	if( black_th > 100 ) { black_th = 100; }
	else if( black_th < 0 ) { black_th = 0; }
	G_BLACK_THRESHOLD = black_th;
}

void linetrace_curve_detect_set_white_threshold( int white_th )
{
	if( white_th > 100 ) { white_th = 100; }
	else if( white_th < 0 ) { white_th = 0; }
	G_WHITE_THRESHOLD = white_th;
}

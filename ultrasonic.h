#ifndef ULTRASONIC_H
#define ULTRASONIC_H

/* 定数定義 */
typedef enum{
	EN_US_STATE_LINETRACE = 0,
	EN_US_STATE_DODGE_OBJECT,
}EN_US_STATE;

/* 外部公開関数定義 */
extern void ultrasonic(void);	/* 超音波センサーによる障害物検地時動作 */

#endif

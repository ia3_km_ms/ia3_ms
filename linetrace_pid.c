#include "ev3api.h"

#include "linetrace_pid.h"

/*========================= 説明 =========================*/

/* P制御：現在値と目標値の偏差に比例。入力値に対して出力値は一定。 */
/* I制御：積分時間によって決まる。時間を長くすると目標値になるまで時間が掛かるが、短くしすぎると収束しない。 */
/* D制御：急激な変化が起こった場合にその変化に抗おうとする。変化の大きさに比例。 */



/* 実験環境で適切なパラメータが見つかったら、本番では目標値の調整だけでいい？ */

/*========================= PIDパターン0 =========================*/
/* 非PID制御 */
/* 変数 */
static int G_pat0_black_threshold = 0;
static int G_pat0_speed = 0;
static int G_pat0_steer = 0;

/* セット */
void linetrace_pid_pat0_set_black_threashold( int black_th )	{ G_pat0_black_threshold = black_th; }
void linetrace_pid_pat0_set_speed( int speed )					{ G_pat0_speed = speed; }
void linetrace_pid_pat0_set_steer( int steer )					{ G_pat0_steer = steer; }

/* 初期化 */
void linetrace_pid_pat0_init( void )
{
	/* 何もしない */
}

/* 角度計算 */
LINETRACE_PID_PARA linetrace_pid_pat0_calc( int reflect )
{
	LINETRACE_PID_PARA ret;
	
	/* 速度設定 */
	ret.speed = G_pat0_speed;
	
	/* 角度設定 */
	if( reflect <= G_pat0_black_threshold )
	{
		/* 黒判定時 */
		ret.steer = G_pat0_steer;
	}
	else
	{
		/* 白判定時 */
		ret.steer = -1 * G_pat0_steer;
	}
	
	return ret;
}

/* リセット */
void linetrace_pid_pat0_reset(void)
{
	//何もしない
}

/*========================= PIDパターン1 =========================*/
/* ライントレースサンプルのPID */
/* 初期化時パラメータ */
static double PAT1_P = 2.0;
static double PAT1_I = 2.0;
static double PAT1_D = 0.5;

static double G_pat1_black = 10;
static double G_pat1_white = 80;
static double G_pat1_midpoint = 0;
static int G_pat1_speed = 100;
static double G_pat1_error_coef = 1.0;

/* 走行時パラメータ */
static double G_pat1_lasterror = 0;
static double G_pat1_integral = 0;

/* セット */
void linetrace_pid_pat1_set_black_target( int black )	{ G_pat1_black = black; G_pat1_midpoint = (G_pat1_white - G_pat1_black) / 2 + G_pat1_black; }
void linetrace_pid_pat1_set_white_target( int white )	{ G_pat1_white = white; G_pat1_midpoint = (G_pat1_white - G_pat1_black) / 2 + G_pat1_black; }
void linetrace_pid_pat1_set_speed( int speed )			{ G_pat1_speed = speed; }
void linetrace_pid_pat1_set_kp( int kp )				{ PAT1_P= (double)kp / 100.0; }  /* 0.01刻み */
void linetrace_pid_pat1_set_ki( int ki )				{ PAT1_I= (double)ki / 100.0; }  /* 0.01刻み */
void linetrace_pid_pat1_set_kd( int kd )				{ PAT1_D= (double)kd / 100.0; }  /* 0.01刻み */
void linetrace_pid_pat1_set_error_coef( int coef )		{ G_pat1_error_coef = (double)coef / 100.0; }  /* 0.01刻み */

/* 初期化 */
void linetrace_pid_pat1_init( void )
{
	/* 反射光の目標値をセット(黒と白の中間値) */
	G_pat1_midpoint = (G_pat1_white - G_pat1_black) / 2 + G_pat1_black;
}

/* 角度計算 */
LINETRACE_PID_PARA linetrace_pid_pat1_calc( int reflect )
{
	double error = G_pat1_error_coef * ( G_pat1_midpoint - reflect );
	G_pat1_integral = error + G_pat1_integral * 0.5;
	double steer = PAT1_P * error + PAT1_I * G_pat1_integral + PAT1_D * ( error - G_pat1_lasterror );
	G_pat1_lasterror = error;
	
	char buf[256];
	//printf("err=%5d, l_err=%5d, int=%5d", (int)error, (int)G_pat1_lasterror, (int)G_pat1_integral);
	sprintf(buf, "err=%5d, l_err=%5d, int=%5d", (int)error, (int)G_pat1_lasterror, (int)G_pat1_integral);
	ev3_lcd_draw_string(buf, 0, 30);
	
	LINETRACE_PID_PARA ret= { G_pat1_speed, (int)steer };
	
	return ret;
}

/* リセット */
void linetrace_pid_pat1_reset( void )
{
	G_pat1_lasterror = 0;
	G_pat1_integral = 0;
}

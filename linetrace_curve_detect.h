#ifndef LINETRACE_CURVE_DETECT_H
#define LINETRACE_CURVE_DETECT_H

#include "stdbool.h"

typedef enum
{
	CURVE_DETECT_NONE = 0,
	CURVE_DETECT_BLACK,
	CURVE_DETECT_WHITE
} CURVE_DETECT;

void linetrace_curve_detect_init( int black_threshold );
int  linetrace_curve_detect( int reflect );

/* パラメータ設定 */
void linetrace_curve_detect_set_detect_frame( int frame );
void linetrace_curve_detect_set_black_threshold( int black_th );
void linetrace_curve_detect_set_white_threshold( int white_th );

#endif
#include <stdio.h>
/*
 *		共通ヘッダ
 */

/* 汎用マクロ定義 */
#define DEBUG
#ifdef DEBUG
#define DEBUG_PRINT(fmt, ...)\
	printf("[%s:%d]"fmt"\n", __func__,__LINE__,##__VA_ARGS__)
#else
#define DEBUG_PRINT(fmt, ...) /* 処理なし */
#endif /* DEBUG */

/* 汎用定義 */
#define D_IA3_MS_OK	(0)
#define D_IA3_MS_NG	(1)

/* センサーポート番号 */
										/* EV3_PORT_1 空き番 */
#define D_PORT_TYPE_ULTRASONIC_SENSOR	EV3_PORT_2
#define D_PORT_TYPE_COLOR_SENSOR		EV3_PORT_3
										/* EV3_PORT_4 空き番 */

/* モーターポート番号 */
#define D_PORT_TYPE_STEER_MOTOR		EV3_PORT_A
#define D_PORT_TYPE_LEFT_MOTOR		EV3_PORT_B
#define D_PORT_TYPE_RIGHT_MOTOR		EV3_PORT_C
									/* EV3_PORT_D 空き番 */


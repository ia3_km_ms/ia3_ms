#include "ev3api.h"

#include "common.h"
#include "drive_control.h"

static const int STEER_MOTOR_ROTATE_MAX = 45;	/* ステアリングモータ最大回転角 */
static const double SPEED_STEER_COEF = 1.0;

static int SPEED_STEP = 2;
static int TURN_RATIO_STEP = 2;

/* 内部変数 */
static int G_speed_setting = 0;
static int G_steer_setting = 0;
static int G_turn_ratio = 0;

static int G_speed_target = 0;
static int G_turn_ratio_target = 0;

/* セット */
void drive_control_set_speed_step( int step ) { SPEED_STEP = step; }
void drive_control_set_turn_ratio_step( int step ) { TURN_RATIO_STEP = step; }

int drive_init(void)
{
	ER ret = E_OK; /* 戻り値 */
	
	ret = ev3_motor_config( D_PORT_TYPE_LEFT_MOTOR, LARGE_MOTOR );
	
	if( ret != E_OK)
	{
		printf("[%s:%d]ret=%d\n",__func__,__LINE__,ret);
		return D_IA3_MS_NG;
	}

	ret = ev3_motor_config( D_PORT_TYPE_RIGHT_MOTOR, LARGE_MOTOR );
	if( ret != E_OK)
	{
		printf("[%s:%d]ret=%d\n",__func__,__LINE__,ret);
		return D_IA3_MS_NG;
	}
	
	ret = ev3_motor_config( D_PORT_TYPE_STEER_MOTOR, MEDIUM_MOTOR );
	if( ret != E_OK)
	{
		printf("[%s:%d]ret=%d\n",__func__,__LINE__,ret);
		return D_IA3_MS_NG;
	}
	
	/* ステアリング回転角初期化 */
#if 0
	ret = ev3_motor_reset_counts( D_PORT_TYPE_STEER_MOTOR );
	if( ret != E_OK)
	{
		printf("[%s:%d]ret=%d\n",__func__,__LINE__,ret);
		return D_IA3_MS_NG;
	}
#endif
	drive_steer(0);
	
	return D_IA3_MS_OK;
}

/* 暫定 */
static inline void CHECK( const char* str, int val )
{
	//printf("%s = %d\n", str, val);
}

/* 速度設定(-100～100) 正は前進、負は後退 */
void drive_speed(int speed)
{
	int step = 0;
	
	/* 目標速度更新 */
	G_speed_target = speed;
	
	/* 速度変更 */
	step = G_speed_target - G_speed_setting;
	
	if( step > SPEED_STEP ) { step = SPEED_STEP; }
	else if( step < (-1 * SPEED_STEP) ) { step = (-1 * SPEED_STEP); }
	
	//G_speed_setting = speed;
	G_speed_setting += step;
	
	//CHECK( "ev3_motor_steer", ev3_motor_steer( D_PORT_TYPE_LEFT_MOTOR, D_PORT_TYPE_RIGHT_MOTOR, G_speed_setting, 0 ) );
	
	CHECK( "ev3_motor_steer", ev3_motor_steer( D_PORT_TYPE_LEFT_MOTOR, D_PORT_TYPE_RIGHT_MOTOR, G_speed_setting, G_turn_ratio ) );
}

/* 舵角設定(-100～100) 正は右転回、負は左転回 */
void drive_steer(int steer)
{
	//printf("drive_steer %d\n", steer);
	
	int current_steer;
	
	/* 最大回転角以上のとき最大回転角に切り捨て */
	if( steer < -1 * STEER_MOTOR_ROTATE_MAX )
	{
		steer = -1 * STEER_MOTOR_ROTATE_MAX;
	}
	else if( steer > STEER_MOTOR_ROTATE_MAX )
	{
		steer = STEER_MOTOR_ROTATE_MAX;
	}
	
	/* 目標角度を更新 */
	G_steer_setting = -1.0 * steer;
	
	/* 現在の角度を取得 */
	current_steer = ev3_motor_get_counts( D_PORT_TYPE_STEER_MOTOR );
	
	/* 現在角度と目標角度の差分だけ回転させる(ノンブロック) */
	CHECK( "ev3_motor_rotate", ev3_motor_rotate( D_PORT_TYPE_STEER_MOTOR, G_steer_setting - current_steer, 100, false ) );
	
	CHECK( "ev3_motor_steer", ev3_motor_steer( D_PORT_TYPE_LEFT_MOTOR, D_PORT_TYPE_RIGHT_MOTOR, G_speed_setting, G_turn_ratio ) );
	
	char buf[256];
	sprintf(buf, "steer=%4d, current=%4d", steer, current_steer);
	ev3_lcd_draw_string(buf, 0, 50);
}

//
void drive_turn_ratio(int ratio)
{
	int step = 0;
	
	/* 目標速度比更新 */
	G_turn_ratio_target = -1 * ratio;
	
	/* 速度比変更 */
	step = G_turn_ratio_target - G_turn_ratio;
	
	if( step > TURN_RATIO_STEP ) { step = TURN_RATIO_STEP; }
	else if( step < (-1 * TURN_RATIO_STEP) ) { step = (-1 * TURN_RATIO_STEP); }
	
	//G_turn_ratio = -1 * ratio;
	G_turn_ratio += step;
}

/*======================== 速度ステアリング ========================*/
/* 速度設定(-100～100) 正は前進、負は後退 */
void drive_speed_by_speed_steering(int speed)
{
	printf("drive_speed %d\n", speed);
	
	G_speed_setting = speed;
	
	CHECK( "ev3_motor_steer", ev3_motor_steer( D_PORT_TYPE_LEFT_MOTOR, D_PORT_TYPE_RIGHT_MOTOR, G_speed_setting, G_steer_setting ) );
}

/* 舵角設定(-100～100) 正は右転回、負は左転回 */
void drive_steer_by_speed_steering(int steer)
{
	printf("drive_steer %d\n", steer);
	
	G_steer_setting = steer;
	
	CHECK( "ev3_motor_steer", ev3_motor_steer( D_PORT_TYPE_LEFT_MOTOR, D_PORT_TYPE_RIGHT_MOTOR, G_speed_setting, G_steer_setting ) );
}


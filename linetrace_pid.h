#ifndef LINETRACE_PID_H
#define LINETRACE_PID_H

typedef struct
{
	int speed;
	int steer;
} LINETRACE_PID_PARA;

/*========================= PIDパターン0 =========================*/
/* 非PID制御 */
void linetrace_pid_pat0_set_black_threashold( int black_th );
void linetrace_pid_pat0_set_speed( int speed );
void linetrace_pid_pat0_set_steer( int steer );

void linetrace_pid_pat0_init( void );
LINETRACE_PID_PARA linetrace_pid_pat0_calc( int reflect );
void linetrace_pid_pat0_reset( void );

/*========================= PIDパターン1 =========================*/
/* ライントレースサンプルのPID */
void linetrace_pid_pat1_set_black_target( int black );
void linetrace_pid_pat1_set_white_target( int white );
void linetrace_pid_pat1_set_speed( int speed );
void linetrace_pid_pat1_set_kp( int kp );
void linetrace_pid_pat1_set_ki( int ki );
void linetrace_pid_pat1_set_kd( int kd );
void linetrace_pid_pat1_set_error_coef( int coef );

void linetrace_pid_pat1_init( void );
LINETRACE_PID_PARA linetrace_pid_pat1_calc( int reflect );
void linetrace_pid_pat1_reset( void );

/*========================= パターン呼び分け用関数マクロ =========================*/
#define LINETRACE_PID_PAT_INIT(n) LINETRACE_PID_PAT_INIT_IMPL(n)
#define LINETRACE_PID_PAT_INIT_IMPL(n) linetrace_pid_pat##n##_init()

#define LINETRACE_PID_PAT_CALC(n, x) LINETRACE_PID_PAT_CALC_IMPL(n, x)
#define LINETRACE_PID_PAT_CALC_IMPL(n, x) linetrace_pid_pat##n##_calc(x)

#define LINETRACE_PID_PAT_RESET(n) LINETRACE_PID_PAT_RESET_IMPL(n)
#define LINETRACE_PID_PAT_RESET_IMPL(n) linetrace_pid_pat##n##_reset()

#endif

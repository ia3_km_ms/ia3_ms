#ifndef CONFIG_H
#define CONFIG_H

/*関数のID*/
typedef enum{
	EN_FUNC_ID_SET_KP = 0,
	EN_FUNC_ID_SET_KI,
	EN_FUNC_ID_SET_KD,
	EN_FUNC_ID_SET_BLACK,
	EN_FUNC_ID_SET_WHITE,
	EN_FUNC_ID_SET_SPEED,
	EN_FUNC_ID_SET_CV_SPEED,
	EN_FUNC_ID_SET_TR,
	EN_FUNC_ID_SET_DT_FRAME,
	EN_FUNC_ID_SET_SPEED_STEP,
	EN_FUNC_ID_SET_TURN_RATIO_STEP,
	EN_FUNC_ID_MAX								/*関数ID最大値*/
}EN_FUNC_ID;


#define INFO_SIZE 256

/*引数を格納した構造体*/
#if 0
typedef struct{
	char charArgs;
	const char* str;
	int intAgs;
	unsigned int u_intArgs;
	short shortArgs;
	unsigned short u_shortArgs;
	long longArgs;
	unsigned long u_longArgs;
}DisposeArgment;
#endif

#if 0
/*関数に設定する値*/
typedef struct{
	union{
		int rightArgs;
		int leftArgs;
	}motor_turn;
	union{
		int speed;
	}motor_drive;
}S_set_value;
#endif


/*関数テーブルの構造体*/
typedef struct{
	EN_FUNC_ID funcId;
	char* funcName;
	int min;
	int max;
	int def;
	void (*func)(int arg);
}funcTable;


/*関数宣言*/
extern void conf_set_value(void);

#endif
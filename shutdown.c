#include "ev3api.h"
#include "shutdown.h"
#include "config_set.h"
#include "linetrace_pid.h"

/* シャットダウンイベント関数 処理終了する */
void button_shutdown_event(intptr_t button)
{
	//funcTable table;
	
	//table = conf_set_value_if();
	
	FILE *file;
	int i = 0;
	
	printf("shutdown start. \n");
	
	// ファイルオープン
	// 最初からファイルを作っておく
	file = fopen("/setting.csv","a");
	
	for( i = 0 ; i < EN_FUNC_ID_MAX ; i++ )
	{
		//fprintf(file, "\n%s,%d", table[i].funcName, table[i].value);
		fprintf(file, "testwrite");
	}
	
	// ファイルクローズ
	fclose(file);
	
	/*- 音を鳴らす */
	ev3_speaker_set_volume(3);
	ev3_speaker_play_tone(NOTE_C4, 1000);
	
	printf("!! SHUTDOWN !!");
	
	ext_tsk();
}

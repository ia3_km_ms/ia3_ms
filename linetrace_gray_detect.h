#ifndef LINETRACE_GLAY_DETECT_H
#define LINETRACE_GLAY_DETECT_H

#include "stdbool.h"

bool linetrace_gray_detect( int reflect );

#endif

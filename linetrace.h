#ifndef LINETRACE_H
#define LINETRACE_H

void linetrace_init(void);
void linetrace_control(void);	/* ライントレース制御(ループ毎にコール) */
void linetrace_reset(void);		/* 内部状態のリセット */

/* セット関数*/
void linetrace_set_black( int black );
void linetrace_set_white( int white );
void linetrace_set_speed( int speed );
void linetrace_set_curve_speed( int speed );
void linetrace_set_curve_turn_ratio( int ratio );

#endif
